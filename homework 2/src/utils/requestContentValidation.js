module.exports = (() => {
    const XRegExp = require('xregexp');
    const wordValidRegex = XRegExp(
        `(?<word> ^[a-zA-Z]{1,50}$) #word`, 'x');

    const validateContent = content => {
        let match = XRegExp.exec(content, wordValidRegex);
        console.log(match);
        if (!match || !match.word) {
            return false;
        }
        return true;
    }

    return {
        validateContent
    };
})();
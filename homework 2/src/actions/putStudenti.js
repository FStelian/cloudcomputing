module.exports = (() => {
    const db = require("../db/database");
    const { writeResponse } = require("../router");
    const requestUtils = require("../utils/requestUtils");
    const validator = require("../utils/requestContentValidation");

	const putStudenti = (request, response) => {
        let id = '';
        if (request.param) {
            id = request.param;
        } else {
            writeResponse(
                {
                    header: {
                        status: 405,
                        reason: "Method Not Allowed"
                    },
                    body: JSON.stringify({
                        status: 405,
                        response: "Method Not Allowed"
                    })
                },
                response
            );
            return;
        }
        requestUtils.getBody(request)
            .then(body => {
                body = JSON.parse(body);
                console.log(body);
                if (!body.nume || !body.prenume) {
                    writeResponse(
                        {
                            header: {
                                status: 400,
                                reason: "Bad Request"
                            },
                            body: JSON.stringify({
                                status: 400,
                                response: "Bad Request"
                            })
                        },
                        response
                    );
                    return new Promise((resolve, refuse) => {
                        refuse("No Content");
                    });
                }
                if (!validator.validateContent(body.nume) || !validator.validateContent(body.prenume)) {
                    writeResponse(
                        {
                            header: {
                                status: 400,
                                reason: "Bad Request"
                            },
                            body: JSON.stringify({
                                status: 400,
                                response: "Bad Request"
                            })
                        },
                        response
                    );
                    return new Promise((resolve, refuse) => {
                        refuse("Bad Request");
                    });
                }
                return db
                    .doQuery(
                        `UPDATE studenti SET nume = ?, prenume = ? WHERE id = ?`,
                        [body.nume, body.prenume, id]
                    ).then((results) => {
                        if (results.affectedRows > 0) {
                            writeResponse(
                                {
                                    header: {
                                        status: 200,
                                        reason: "OK"
                                    },
                                    body: JSON.stringify({
                                        status: 200,
                                        response: "OK"
                                    })
                                },
                                response
                            );
                        } else {
                            writeResponse(
                                {
                                    header: {
                                        status: 404,
                                        reason: "Not Found"
                                    },
                                    body: JSON.stringify({
                                        status: 404,
                                        response: "Not Found"
                                    })
                                },
                                response
                            );
                        }
                    }).catch(error => {
                        console.log(error);
                        writeResponse(
                            {
                                header: {
                                    status: 500,
                                    reason: "Internal Server Error"
                                },
                                body: JSON.stringify({
                                    status: 500,
                                    response: "Internal Server Error"
                                })
                            },
                            response
                        );
                    });
            }).catch(error => {
                console.log(error);
                writeResponse(
                    {
                        header: {
                            status: 500,
                            reason: "Internal server error"
                        },
                        body: JSON.stringify({
                            status: 500,
                            response: "Internal server error"
                        })
                    },
                    response
                );
        });
    };

	return putStudenti;
})();


module.exports = (() => {
    const db = require("../db/database");
    const { writeResponse } = require("../router");

	const deleteStudenti = (request, response) => {
        let id = '';
        let query = 'DELETE FROM studenti';
        if (request.param) {
            id = request.param;
            let query = 'DELETE FROM studenti WHERE id = ?';
        }
        return db
            .doQuery(
                query, [id] 
            ).then((results) => {
                if (results.affectedRows > 0) {
                    writeResponse(
                        {
                            header: {
                                status: 200,
                                reason: "OK"
                            },
                            body: JSON.stringify({
                                status: 200,
                                response: "OK"
                            })
                        },
                        response
                    );
                } else {
                    writeResponse(
                        {
                            header: {
                                status: 404,
                                reason: "Not Found"
                            },
                            body: JSON.stringify({
                                status: 404,
                                response: "Not Found"
                            })
                        },
                        response
                    );
                }
            }).catch(error => {
                console.log(error);
                writeResponse(
                    {
                        header: {
                            status: 500,
                            reason: "Internal server error"
                        },
                        body: JSON.stringify({
                            status: 500,
                            response: "Internal server error"
                        })
                    },
                    response
                );
            });
    };

	return deleteStudenti;
})();


module.exports = (() => {
    const db = require("../db/database");
    const { writeResponse } = require("../router");
    const requestUtils = require("../utils/requestUtils");
    const validator = require("../utils/requestContentValidation");

	const postStutdenti = (request, response) => {
        let id = 'default';
        if (request.param) {
            id = request.param;
        }
        requestUtils.getBody(request)
            .then(body => {
                body = JSON.parse(body);
                console.log(body);
                if (!body.nume || !body.prenume) {
                    writeResponse(
                        {
                            header: {
                                status: 400,
                                reason: "Bad Request"
                            },
                            body: JSON.stringify({
                                status: 400,
                                response: "Bad Request"
                            })
                        },
                        response
                    );
                    return new Promise((resolve, refuse) => {
                        refuse("No Content");
                    });
                }
                if (body.length > 0) {
                    writeResponse(
                        {
                            header: {
                                status: 405,
                                reason: "Method Not Allowed"
                            },
                            body: JSON.stringify({
                                status: 405,
                                response: "Method Not Allowed"
                            })
                        },
                        response
                    );
                    return;
                }
                if (!validator.validateContent(body.nume) || !validator.validateContent(body.prenume)) {
                    writeResponse(
                        {
                            header: {
                                status: 400,
                                reason: "Bad Request"
                            },
                            body: JSON.stringify({
                                status: 400,
                                response: "Bad Request"
                            })
                        },
                        response
                    );
                    return new Promise((resolve, refuse) => {
                        refuse("Bad Request");
                    });
                }
                return db
                    .doQuery(
                        `INSERT INTO studenti VALUES (?, ?, ?)`,
                        [id, body.nume, body.prenume]
                    ).then((results) => {
                        writeResponse(
                            {
                                header: {
                                    status: 201,
                                    reason: "Created"
                                },
                                body: JSON.stringify({
                                    status: 201,
                                    response: "Created",
                                    results: results.insertId
                                })
                            },
                            response
                        );
                    }).catch(error => {
                        console.log(error);
                        writeResponse(
                            {
                                header: {
                                    status: 409,
                                    reason: "Conflict"
                                },
                                body: JSON.stringify({
                                    status: 409,
                                    response: "Conflict"
                                })
                            },
                            response
                        );
                    });
            }).catch(error => {
                console.log(error);
                writeResponse(
                    {
                        header: {
                            status: 500,
                            reason: "Internal server error"
                        },
                        body: JSON.stringify({
                            status: 500,
                            response: "Internal server error"
                        })
                    },
                    response
                );
        });
    };

	return postStutdenti;
})();


module.exports = (() => {
    const db = require("../db/database");
    const { writeResponse } = require("../router");

	const getStudenti = (request, response) => {
        let id = '';
        let query = `SELECT * FROM studenti`;
        if (request.param) {
            id = request.param;
            query = `SELECT * FROM studenti WHERE ID = ?`;
        }
        return db
            .doQuery(
                query, [id] 
            ).then((results) => {
                if (results.length > 0) {
                    writeResponse(
                        {
                            header: {
                                status: 200,
                                reason: "OK"
                            },
                            body: JSON.stringify({
                                status: 200,
                                response: "OK",
                                results: results
                            })
                        },
                        response
                    );
                } else {
                    writeResponse(
                        {
                            header: {
                                status: 404,
                                reason: "Not Found"
                            },
                            body: JSON.stringify({
                                status: 404,
                                response: "Not Found"
                            })
                        },
                        response
                    );
                }
            }).catch(error => {
                console.log(error);
                writeResponse(
                    {
                        header: {
                            status: 500,
                            reason: "Internal server error"
                        },
                        body: JSON.stringify({
                            status: 500,
                            response: "Internal server error"
                        })
                    },
                    response
                );
            });
    };

	return getStudenti;
})();


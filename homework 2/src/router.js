module.exports = (() => {
    var parser = require('url');
    const XRegExp = require('xregexp');
    const urlAddRegex = XRegExp(
        `(?<url> \/\\w+) #url
        (\/:(?<param> \\w+))* #param`, 'x');
    const urlReqRegex = XRegExp(
        `(?<url> \/\\w+) #url
        (\/(?<param> \\d+))* #param
        (?<error>.+)* #wrong-param`, 'x');
    let routes = {};

    const addRoute = ({ authed, method, url, fn }) => {
        let match = XRegExp.exec(url, urlAddRegex);
        url = match.url;
        if (routes[method] === undefined) {
            routes[method] = {};
        }
        if (routes[method][url] === undefined) {
            routes[method][url] = {
                fn: fn,
                authed: authed,
                "param": match.param
            };
        }
    };

    const route = (request, response) => {
        let { method: reqMethod, url: reqUrl, headers: reqHeaders } = request;
		var parseUrl = parser.parse(reqUrl,true);
        var parseData = parseUrl.query;
        let match = XRegExp.exec(parseUrl.pathname, urlReqRegex);
        reqUrl = match.url;
        request.params = {};
        console.log(match);
        request.param = match.param;
        url = match.url;
        if (match.error) {
            writeResponse(
                {
                    header: {
                        status: 400,
                        reason: "Bad Request"
                    },
                    body: JSON.stringify({
                        status: 400,
                        response: "Bad Request"
                    })
                },
                response
            );
        } else {
            if (
                routes[reqMethod] !== undefined &&
                routes[reqMethod][reqUrl] !== undefined
            ) {
                routeNotAuthed(
                    request,
                    response,
                    routes[reqMethod][reqUrl]["fn"]
                );  
            } else {
                writeResponse(
                    {
                        header: {
                            status: 404
                        },
                        body: JSON.stringify({
                            status: "error",
                            message: "Route/item not found."
                        })
                    },
                    response
                );
            }
        }
    };

    const use = callback => {
        callback().forEach(item => {
            if (routes[item.method] === undefined) {
                routes[item.method] = {};
            }
            if (routes[item.method][item.url] === undefined) {
                routes[item.method][item.url] = {
                    fn: item.fn,
                    authed: item.authed
                };
            }
        });
    };

    const writeResponse = function({ header, body }, response) {
        if (!response.finished) {
            response.writeHead(header.status, header.reason, header.headers);
            console.log(body);
            response.write(body);
            response.end();
        }
    };

    const routeNotAuthed = (request, response, doThis) => {
        return doThis(request, response);
    };

    return {
        addRoute,
        route,
        use,
        writeResponse
    };
})();

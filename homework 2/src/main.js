var http = require("http");
var router = require("./router");
var postStudenti = require('./actions/postStudenti');
var getStudenti = require('./actions/getStudenti');
var deleteStudenti = require('./actions/deleteStudenti');
var putStudenti = require('./actions/putStudenti');

var initRoutes = () => {
    router.addRoute({
        method: "POST",
        url: "/studenti/:id",
        fn: postStudenti
    });

    router.addRoute({
        method: "GET",
        url: "/studenti/:id",
        fn: getStudenti
    });

    router.addRoute({
        method: "DELETE",
        url: "/studenti/:id",
        fn: deleteStudenti
    });

    router.addRoute({
        method: "PUT",
        url: "/studenti/:id",
        fn: putStudenti
    });
};

var server = http.createServer(function(req, res) {
    router.route(req, res);
});

initRoutes();
server.listen(8081);
console.log("Listening at http://localhost:8081");

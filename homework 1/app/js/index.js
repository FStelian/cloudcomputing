let resultSpan = document.getElementById('api-result');

function randomNumberAPI() {
    fetch('/randomNumberAPI')
    .then(data => data.json())
    .then(body => {
        resultSpan.innerHTML = body.response;
    })
}

function randomWordAPI() {
    fetch('/randomWordAPI')
    .then(data => data.json())
    .then(body => {
        resultSpan.innerHTML = body.response;
    })
}

function randomFragmentAPI() {
    fetch('/randomFragmentAPI')
    .then(data => data.json())
    .then(body => {
        resultSpan.innerHTML = body.response;
    })
}

function threeRandomAPIs() {
    fetch('/threeRandomAPIs')
    .then(data => data.json())
    .then(body => {
        resultSpan.innerHTML = body.response;
    })
}
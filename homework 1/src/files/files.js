module.exports = (() => {
    const fs = require("fs");
    const mime = require("./mime.js");
    const authedFiles = require("./fileList");

    const staticFiles = () => {
        let routes = [];

        authedFiles.notAuthed.forEach(item => {
            routes.push({
                method: "GET",
                url: item,
                fn: serveFile
            });
        });

        return routes;
    };

    const serveFile = (request, response) => {
        const stream = fs.createReadStream("./app" + request.url);
        stream.on("open", () => {
            response.writeHead(200, {
                "Content-type": mime.getMime(request.url)
            });
            stream.pipe(response);
        });
        stream.on("error", () => {
            response.writeHead(404, {
                "Content-type": "text/html"
            });
            // stream.pipe(response);
            response.end();
        });
    };

    return {
        staticFiles,
        serveFile
    };
})();

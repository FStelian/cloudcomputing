var http = require("http");
var router = require("./router");
var files = require("./files/files");
var randomWordAPI = require('./actions/randomWordAPI');
var randomFragmentAPI = require('./actions/randomFragmentAPI');
var randomNumberAPI = require('./actions/randomNumberAPI');
var threeRandomAPIs = require('./actions/threeRandomAPIs');
var metrics = require('./actions/metrics');
var index = require('./actions/index');

var initRoutes = () => {
    router.use(files.staticFiles);

    router.addRoute({
        method: "GET",
        url: "/metrics",
        fn: metrics
    });

    router.addRoute({
        method: "GET",
        url: "/randomNumberAPI",
        fn: randomNumberAPI
    });

    router.addRoute({
        method: "GET",
        url: "/randomWordAPI",
        fn: randomWordAPI
    });

    router.addRoute({
        method: "GET",
        url: "/randomFragmentAPI",
        fn: randomFragmentAPI
    });

    router.addRoute({
        method: "GET",
        url: "/threeRandomAPIs",
        fn: threeRandomAPIs
    });

    router.addRoute({
        method: "GET",
        url: "/index",
        fn: index
    });
};

var server = http.createServer(function(req, res) {
    router.route(req, res);
});

initRoutes();
server.listen(8081);
console.log("Listening at http://localhost:8081");

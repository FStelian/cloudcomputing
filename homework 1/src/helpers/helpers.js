module.exports = (() => {
    const getBody = request => {
        return new Promise((resolve, reject) => {
            let data = "";
            request.on("data", chunk => (data += chunk));
            request.on("end", () => resolve(data));
            request.on("error", error => reject(error));
        });
    };

    const parseBody = data => {
        return new Promise((resolve, reject) => {
            try {
                resolve(JSON.parse(data));
            } catch (error) {
                reject(error);
            }
        });
    };

    return {
        getBody,
        parseBody
    }
})();
module.exports = (() => {
    const mysql = require("mysql");

    const db = mysql.createPool({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'cloudcomputing'
    });

    const doQuery = (query, queryParams) => {
        return new Promise((resolve, refuse) => {
            db.getConnection((err, connection) => {
                if (err) {
                    refuse(err);
                }
                connection.query(query, queryParams, (err, results) => {
                    connection.release();
                    if (err) {
                        refuse(err);
                    }
                    else {
                        resolve(results);
                    }
                })
            })
        });
    };

    return {
        doQuery
    }
})();
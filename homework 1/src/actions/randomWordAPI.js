module.exports = (() => {
	const requestLib = require('request');
	const { writeResponse } = require("../router");
	const fs = require('fs');
    const path = require('path');    
    const filePath = path.join(__dirname, '../config.txt');
	let key = fs.readFileSync(filePath, 'utf8');
	const db = require("../db/database");

	const randomWordAPI = (request, response) => {
		requestLib('https://random-word-api.herokuapp.com/word?key=' + key + '&number=1', { json: true }, (err, res, body) => {
			db.doQuery('insert into logs values(default, ?, ?, ?, ?, default)', 
				['WordsAPI', 'GET', res.statusCode, res.elapsedTime]);
			if (err) {
				writeResponse({
						header: {
							status: res.statusCode,
							reason: err
						},
						body: JSON.stringify({
							status: res.statusCode,
							response: err
						})
					},
					response
				);
			} else {
				writeResponse({
						header: {
							status: 200,
							reason: "OK"
						},
						body: JSON.stringify({
							status: 200,
							response: body[0],
						})
					},
					response
				);
			}
		});
	}

	return randomWordAPI;
})();


module.exports = (() => {
	const mime = require("../files/mime.js");
	const db = require("../db/database");
	var ejs = require("ejs");

	const index = (request, response) => {
        ejs.renderFile('./app/index.ejs', {}, function(err, data){
            console.log(err);
            response.writeHeader(200, {"Content-Type": "text/html"});
            response.write(data);
            response.end();
        });
	}

	return index;
})();


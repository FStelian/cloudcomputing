module.exports = (() => {
	const requestLib = require('request');
	const { writeResponse } = require("../router");
	const fs = require('fs');
    const path = require('path');    
    const filePath = path.join(__dirname, '../config.txt');
	let key = fs.readFileSync(filePath, 'utf8');
	const randomWordAPI = require('./randomWordAPI');
	const randomNumberAPI = require('./randomNumberAPI');
	const randomFragmentAPI = require('./randomFragmentAPI');
	const db = require("../db/database");

	const GetWordByPos = (str, pos) => {
		return str.split(' ')[pos];
	}

	const replaceWord = (str, oldWord, newWord) => {
		let splitStrArray = str.split(oldWord);
		let resultString = '';
		for (let i = 0, len = splitStrArray.length; i < len; i++) {
			if (i != len - 1) {
				resultString += splitStrArray[i] + newWord;
			} else {
				resultString += splitStrArray[i];
			}
		}
		return resultString;
	}

	const threeRandomAPIs = (request, response) => {
		requestLib('http://numbersapi.com/random/trivia', { json: true , time: true }, 
			(errFragmentAPI, resFragmentAPI, bodyFragmentAPI) => {
				db.doQuery('insert into logs values(default, ?, ?, ?, ?, default)', 
				['FragmentAPI', 'GET', resFragmentAPI.statusCode, resFragmentAPI.elapsedTime]);
			requestLib('https://www.random.org/integers/?num=1&min=1&max=5&col=1&base=10&format=plain&rnd=new', { json: true, time: true}, 
				(errNumbersAPI, resNumbersAPI, bodyNumbersAPI) => {
					db.doQuery('insert into logs values(default, ?, ?, ?, ?, default)', 
					['NumbersAPI', 'GET', resNumbersAPI.statusCode, resNumbersAPI.elapsedTime]);
				requestLib('https://random-word-api.herokuapp.com/word?key=' + key + '&number=1', { json: true, time:true }, 
					(errWordsAPI, resWordsAPI, bodyWordsAPI) => {
						db.doQuery('insert into logs values(default, ?, ?, ?, ?, default)', 
						['WordsAPI', 'GET', resWordsAPI.statusCode, resWordsAPI.elapsedTime]);
					let fragmentAPIResult = bodyFragmentAPI + '';
					let numbersAPIResult = bodyNumbersAPI + '';
					let wordAPIResult = bodyWordsAPI[0] + '';
					let wordAtPos = GetWordByPos(fragmentAPIResult, numbersAPIResult);
					fragmentAPIResult = replaceWord(fragmentAPIResult, wordAtPos.trim(), wordAPIResult.trim());

					if(resFragmentAPI.statusCode >= 300 || resNumbersAPI.statusCode >= 300 || 
						resWordsAPI.statusCode >= 300) {
						writeResponse({
							header: {
								status: Math.max(resFragmentAPI.statusCode, resNumbersAPI.statusCode, resWordsAPI.statusCode),
								reason: "API Request failed"
							},
							body: JSON.stringify({
								status: Math.max(resFragmentAPI.statusCode, resNumbersAPI.statusCode, resWordsAPI.statusCode),
								response: "API Request failed"
							})
						},
						response
						);
					} else {
						writeResponse({
							header: {
								status: 200,
								reason: "OK"
							},
							body: JSON.stringify({
								status: 200,
								response: fragmentAPIResult
							})
						},
						response
						);
					}
				});
			});
		});	
	}

	return threeRandomAPIs;
})();


module.exports = (() => {
	const mime = require("../files/mime.js");
	const db = require("../db/database");
	var ejs = require("ejs");

	const metrics = (request, response) => {
        db.doQuery(`SELECT * FROM logs`).then(logs => {
            console.log(logs[1]);
            ejs.renderFile('./app/metrics.ejs', {APIResults: logs}, function(err, data){
                console.log(err);
                response.writeHeader(200, {"Content-Type": "text/html"});
                response.write(data);
                response.end();
            });
        });
	}

	return metrics;
})();


module.exports = (() => {
	const requestLib = require('request');
	const { writeResponse } = require("../router");
	const db = require("../db/database");

	const randomNumberAPI = (request, response) => {
		requestLib('https://www.random.org/integers/?num=1&min=1&max=5&col=1&base=10&format=plain&rnd=new', 
		{ json: true, time: true }, (err, res, body) => {
			console.log(err);
			db.doQuery('insert into logs values(default, ?, ?, ?, ?, default)', 
				['NumbersAPI', 'GET', res.statusCode, res.elapsedTime]);
			if (err) {
				console.log(err);
				writeResponse({
						header: {
							status: res.statusCode,
							reason: err
						},
						body: JSON.stringify({
							status: res.statusCode,
							response: err
						})
					},
					response
				);
			} else {
				writeResponse({
						header: {
							status: 200,
							reason: "OK"
						},
						body: JSON.stringify({
							status: 200,
							response: body,
						})
					},
					response
				);
			}
		});
	}

	return randomNumberAPI;
})();


module.exports = (() => {
	const requestLib = require('request');
	const { writeResponse } = require("../router");
	const db = require("../db/database");

	const randomFragmentAPI = (request, response) => {
		requestLib('http://numbersapi.com/random/trivia', { json: true, time: true }, (err, res, body) => {
			db.doQuery('insert into logs values(default, ?, ?, ?, ?, default)', 
				['FragmentAPI', 'GET', res.statusCode, res.elapsedTime]);
			if (err) {
				writeResponse({
						header: {
							status: res.statusCode,
							reason: err
						},
						body: JSON.stringify({
							status: res.statusCode,
							response: err
						})
					},
					response
				);
			} else {
				writeResponse({
						header: {
							status: 200,
							reason: "OK"
						},
						body: JSON.stringify({
							status: 200,
							response: body,
						})
					},
					response
				);
			}
		});
	}

	return randomFragmentAPI;
})();


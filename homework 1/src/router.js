module.exports = (() => {
	var parser = require('url');
    let routes = {};

    const addRoute = ({ authed, method, url, fn }) => {
        if (routes[method] === undefined) {
            routes[method] = {};
        }
        if (routes[method][url] === undefined) {
            routes[method][url] = {
                fn: fn,
                authed: authed
            };
        }
    };

    const route = (request, response) => {
        let { method: reqMethod, url: reqUrl, headers: reqHeaders } = request;
		
		var parseUrl = parser.parse(reqUrl,true);
		var parseData = parseUrl.query;
		reqUrl = parseUrl.pathname;

        if (
            routes[reqMethod] !== undefined &&
            routes[reqMethod][reqUrl] !== undefined
        ) {
            routeNotAuthed(
                request,
                response,
                routes[reqMethod][reqUrl]["fn"]
            );  
        } else {
            writeResponse(
                {
                    header: {
                        status: 404
                    },
                    body: JSON.stringify({
                        status: "error",
                        message: "Route/item not found."
                    })
                },
                response
            );
        }
    };

    const use = callback => {
        callback().forEach(item => {
            if (routes[item.method] === undefined) {
                routes[item.method] = {};
            }
            if (routes[item.method][item.url] === undefined) {
                routes[item.method][item.url] = {
                    fn: item.fn,
                    authed: item.authed
                };
            }
        });
    };

    const writeResponse = function({ header, body }, response) {
        if (!response.finished) {
            response.writeHead(header.status, header.reason, header.headers);
            response.write(body);
            response.end();
        }
    };

    const routeNotAuthed = (request, response, doThis) => {
        return doThis(request, response);
    };

    return {
        addRoute,
        route,
        use,
        writeResponse
    };
})();
